#C++ string counter based on epoll

##Proto

First of all you need to generate /requestResponseSchema.pb.cc and requestResponseSchema.pb.h. files.

Get into "proto" folder and run protoc compiler by following command:

```
protoc -I=. --cpp_out=./../src requestResponseSchema.proto
```

##Compile and run
To compile and run (you need to install required libraries before) server you need to use following commands:
```
cd src
mkdir build
cd build
cmake ..
make
./esw_server
```

##More
See more on - https://cw.fel.cvut.cz/wiki/courses/b4m36esw/labs/lab06
Project GitLab - https://gitlab.fel.cvut.cz/barysole/b4m36esw-server-in-c
