#ifndef CPP_SEMESTRAL_WORK_GZIPDECODER_H
#define CPP_SEMESTRAL_WORK_GZIPDECODER_H

#include <sstream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

class GzipDecoder {
public:
    static std::string decode(const std::string &gzipData) {
        std::stringstream coded(gzipData);
        std::stringstream decoded;

        boost::iostreams::filtering_streambuf<boost::iostreams::input> outputStream;
        outputStream.push(boost::iostreams::gzip_decompressor());
        outputStream.push(coded);
        boost::iostreams::copy(outputStream, decoded);

        return decoded.str();
    }
};


#endif //CPP_SEMESTRAL_WORK_GZIPDECODER_H
