#ifndef CPP_SEMESTRAL_WORK_BLOCKINGSTRINGSET_H
#define CPP_SEMESTRAL_WORK_BLOCKINGSTRINGSET_H

#include "string"
#include "vector"
#include "unordered_set"
#include "mutex"

class BlockingStringSet {
public:
    BlockingStringSet(int setSize);

    void addAll(std::vector<std::string> *stringArr);

    int getStringCount();

    std::unordered_set<std::string> *stringSet;
    pthread_mutex_t setLock = PTHREAD_MUTEX_INITIALIZER;;
};

#endif //CPP_SEMESTRAL_WORK_BLOCKINGSTRINGSET_H
