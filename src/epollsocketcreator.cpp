#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <cstdio>
#include <cerrno>
#include "epollsocketcreator.h"
#include "epollsocketreader.h"
#include "list"

EpollSocketCreator::EpollSocketCreator(EpollInstance &e, short int portNumber, BlockingStringSet *blockingStringSet)
        : EpollFd(-1, e), blockingStringSet(blockingStringSet) {
    fd = socket(AF_INET, SOCK_STREAM, 0);
    int optval = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));
    struct sockaddr_in socketAddress;

    memset(&socketAddress, 0, sizeof(socketAddress));
    socketAddress.sin_family = AF_INET;
    socketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    socketAddress.sin_port = htons(portNumber);
    int bindRes = bind(fd, (struct sockaddr *) &socketAddress, sizeof(socketAddress));
    if (bindRes == -1) {
        char errBuffer[256];
        char *bindError = strerror_r(errno, errBuffer, 256);
        printf("Bind error: %s\n", bindError);
    }
    int flags = fcntl(fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(fd, F_SETFL, flags);
    listen(fd, SOMAXCONN);

    registerFd(EPOLLIN);
}

EpollSocketCreator::~EpollSocketCreator() {
    unregisterFd();
    close(fd);
}

void EpollSocketCreator::handleEvent(uint32_t events) {
    if ((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)) {
        unregisterFd();
    } else {
        int connectionFd = accept(fd, NULL, NULL);
        auto *esr = new EpollSocketReader(epollInstance, connectionFd, blockingStringSet);
    }
}
