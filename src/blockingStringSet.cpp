#include "blockingStringSet.h"
#include "vector"
#include "unordered_set"

BlockingStringSet::BlockingStringSet(int setSize) {
    stringSet = new std::unordered_set<std::string>(setSize);
}

void BlockingStringSet::addAll(std::vector<std::string> *stringArr) {
    pthread_mutex_lock(&setLock);
    (*stringSet).insert((*stringArr).begin(), (*stringArr).end());
    pthread_mutex_unlock(&setLock);
}

int BlockingStringSet::getStringCount() {
    pthread_mutex_lock(&setLock);
    int stringCount = (*stringSet).size();
    (*stringSet).clear();
    pthread_mutex_unlock(&setLock);
    return stringCount;
}

