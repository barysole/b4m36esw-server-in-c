#ifndef EPOLL_SERVER_EPOLLSOCKETCREATOR_H
#define EPOLL_SERVER_EPOLLSOCKETCREATOR_H

#include <sys/epoll.h>
#include <list>
#include "epollfd.h"
#include "epollinstance.h"
#include "epollsocketreader.h"

class EpollSocketCreator : public EpollFd {
public:
    EpollSocketCreator(EpollInstance &e, short int portNumber, BlockingStringSet *blockingStringSet);

    ~EpollSocketCreator();

    void handleEvent(uint32_t events);

    BlockingStringSet *blockingStringSet;
};


#endif //EPOLL_SERVER_EPOLLSOCKETCREATOR_H
