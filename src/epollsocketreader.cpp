#include <unistd.h>
#include "epollsocketreader.h"
#include <fcntl.h>
#include <iostream>
#include "requestResponseSchema.pb.h"
#include "blockingStringSet.h"
#include "gzipDecoder.h"
#include <boost/tokenizer.hpp>

EpollSocketReader::EpollSocketReader(EpollInstance &e, int connectionFd, BlockingStringSet *blockingStringSet)
        : EpollFd(connectionFd, e), blockingStringSet(blockingStringSet) {
    int flags = fcntl(connectionFd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(connectionFd, F_SETFL, flags);

    registerFd(EPOLLIN);
}

EpollSocketReader::~EpollSocketReader() {
    unregisterFd();
    close(fd);
}

int EpollSocketReader::readMessageSizeFromFd(int fd) {
    long count;
    if (byteRead == 0) {
        count = read(fd, inputBuffer, 4);
        if (count == 0) {
            return 0;
        }
        byteRead = count;
    }
    if (byteRead != 4) {
        while (byteRead != 4) {
            count = read(fd, inputBuffer + byteRead, 4 - byteRead);
            if (count < 0) {
                return 0;
            } else if (count == 0) {
                return -1;
            }
            byteRead += count;
        }
    }

    return (int) ((unsigned char) (inputBuffer[0]) << 24 |
                  (unsigned char) (inputBuffer[1]) << 16 |
                  (unsigned char) (inputBuffer[2]) << 8 |
                  (unsigned char) (inputBuffer[3]));
}

struct ThreadArguments {
    ThreadArguments(const Request *request, BlockingStringSet *blockingStringSet, int fd) : request(request),
                                                                                            blockingStringSet(
                                                                                                    blockingStringSet),
                                                                                            fd(fd) {}

    const Request *request;
    BlockingStringSet *blockingStringSet;
    int fd;
};

void *processPostWords(void *args) {
    ThreadArguments *threadArguments = ((ThreadArguments *) args);
    const Request *request = (*threadArguments).request;
    BlockingStringSet *blockingStringSet = (*threadArguments).blockingStringSet;
    int fd = (*threadArguments).fd;
    std::string unzippedString = GzipDecoder::decode((*request).postwords().data());
    auto *stringVector = new std::vector<std::string>();
    boost::char_separator<char> separator("    \r\n\t\f");
    boost::tokenizer<boost::char_separator<char>> tokens(unzippedString, separator);
    for (const auto &separateString: tokens) {
        (*stringVector).push_back(separateString);
    }
    (*blockingStringSet).addAll(stringVector);
    Response response;
    response.set_status(Response_Status_OK);
    auto *responseString = new std::string();
    int responseMessageSize = response.ByteSize();
    char *messageSizeArray = (char *) calloc(4, sizeof(char));
    for (int i = 0; i < 4; i++) {
        messageSizeArray[3 - i] = (responseMessageSize >> (i * 8));
    }
    for (int i = 0; i < 4; i++) {
        (*responseString).push_back(messageSizeArray[i]);
    }
    (*responseString) += response.SerializeAsString();
    write(fd, (*responseString).c_str(), (*responseString).size());
    return nullptr;
}

Request *EpollSocketReader::readRequest(int messageSize) {
    if (messageByteRead == 0) {
        messageInputBuffer = (char *) calloc(messageSize, sizeof(char));
    }
    long count;
    while (messageByteRead != messageSize) {
        count = read(fd, &messageInputBuffer[messageByteRead], messageSize - messageByteRead);
        if (count > 0) {
            messageByteRead += count;
        }
        if (count == 0) {
            return nullptr;
        }
    }
    auto *request = new Request();
    (*request).ParseFromArray(messageInputBuffer, messageSize);
    return request;
}

void EpollSocketReader::processGetCountRequest() {
    int stringCount = (*blockingStringSet).getStringCount();
    auto *response = new Response();
    (*response).set_status(Response_Status_OK);
    (*response).set_counter(stringCount);
    auto *responseString = new std::string();
    int responseMessageSize = (*response).ByteSize();
    char *messageSizeArray = (char *) calloc(4, sizeof(char));
    for (int i = 0; i < 4; i++) {
        messageSizeArray[3 - i] = (responseMessageSize >> (i * 8));
    }
    for (int i = 0; i < 4; i++) {
        (*responseString).push_back(messageSizeArray[i]);
    }
    (*responseString) += (*response).SerializeAsString();
    write(fd, (*responseString).c_str(), (*responseString).size());
}

void EpollSocketReader::processPostWordRequest(Request *request) {
    pthread_t threadId;
    auto *threadArguments = new ThreadArguments(request, blockingStringSet, fd);
    if (pthread_create(&threadId, NULL, processPostWords, (void *) threadArguments) != 0) {
        throw std::runtime_error(std::string("creating thread error: ") + std::strerror(errno));
    }
    if (pthread_detach(threadId) != 0) {
        throw std::runtime_error(std::string("detaching thread error:") + std::strerror(errno));
    }
}

void EpollSocketReader::handleEvent(uint32_t events) {
    if ((events & EPOLLERR) || (events & EPOLLHUP) || !(events & EPOLLIN)) {
        unregisterFd();
    } else {
        int messageSize = readMessageSizeFromFd(fd);
        if (messageSize > 0) {
            Request *request = readRequest(messageSize);
            if (request == nullptr) {
                return;
            }
            if ((*request).msg_case() == (*request).kGetCount) {
                processGetCountRequest();
            } else if ((*request).msg_case() == (*request).kPostWords) {
                processPostWordRequest(request);
            }
            byteRead = 0;
        } else if (messageSize == 0) {
            unregisterFd();
        }
    }
}