#ifndef EPOLL_SERVER_EPOLLSOCKETREADER_H
#define EPOLL_SERVER_EPOLLSOCKETREADER_H

#include <sys/epoll.h>
#include "epollfd.h"
#include "blockingStringSet.h"
#include "epollinstance.h"
#include "requestResponseSchema.pb.h"

class EpollSocketReader : public EpollFd {
public:
    EpollSocketReader(EpollInstance &e, int connectionFd, BlockingStringSet *blockingStringSet);

    ~EpollSocketReader();

    void handleEvent(uint32_t events);

    long messageByteRead = 0;
    char *messageInputBuffer;
    BlockingStringSet *blockingStringSet;
    char *inputBuffer = (char *) calloc(4, sizeof(char));
    long byteRead = 0;

    int readMessageSizeFromFd(int fd);

    Request *readRequest(int messageSize);

    void processGetCountRequest();

    void processPostWordRequest(Request *request);
};


#endif //EPOLL_SERVER_EPOLLSOCKETREADER_H
