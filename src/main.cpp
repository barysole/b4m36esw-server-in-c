#include "epollinstance.h"
#include "epollsocketcreator.h"
#include "blockingStringSet.h"

using namespace std;

int main(int argc, char *argv[]) {
    EpollInstance ep;
    EpollSocketCreator epollSocketCreator(ep, 8848, new BlockingStringSet(5000000));

    while (1) {
        ep.waitAndHandleEvents();
    }
}
